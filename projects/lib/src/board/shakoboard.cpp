/*
    This file is part of Cute Chess.
    Copyright (C) 2008-2018 Cute Chess authors

    Cute Chess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cute Chess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cute Chess.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shakoboard.h"
#include "westernzobrist.h"

namespace Chess {

ShakoBoard::ShakoBoard()
	: WesternBoard(new WesternZobrist())
{
    setPieceType(Cannon, tr("cannon"), "C", CannonMovement, "CAN");
    setPieceType(Elephant, tr("elephant"), "E", ElephantMovement, "E");
}

Board* ShakoBoard::copy() const
{
    return new ShakoBoard(*this);
}

QString ShakoBoard::variant() const
{
    return "shako";
}

int ShakoBoard::width() const
{
	return 10;
}

int ShakoBoard::height() const
{
    return 10;
}

void ShakoBoard::vInitialize()
{
    WesternBoard::vInitialize();
    int arwidth = width() + 2;

    m_ferzOffsets.resize(4);
    m_ferzOffsets[0] = -arwidth - 1;
    m_ferzOffsets[1] = -arwidth + 1;
    m_ferzOffsets[2] = arwidth - 1;
    m_ferzOffsets[3] = arwidth + 1;

    m_elephantOffsets.resize(4);
    m_elephantOffsets[0] = -2 * arwidth - 2;
    m_elephantOffsets[1] = -2 * arwidth + 2;
    m_elephantOffsets[2] = 2 * arwidth - 2;
    m_elephantOffsets[3] = 2 * arwidth + 2;
    //Elephant Offsets from Shatranj

    m_orthogonalOffsets.resize(4);
    m_orthogonalOffsets[0] = -arwidth;
    m_orthogonalOffsets[1] = -1;
    m_orthogonalOffsets[2] =  1;
    m_orthogonalOffsets[3] =  arwidth;
    //Cannon taken from https://github.com/gaintpd/cutechess

}

QString ShakoBoard::defaultFenString() const
{
    return "c8c/ernbqkbnre/pppppppppp/10/10/10/10/PPPPPPPPPP/ERNBQKBNRE/C8C w KQkq - 0 1";
}

inline int ShakoBoard::pawnMoveOffset(const PawnStep& ps, int sign) const
{
    return sign * ps.file - sign * (width() + 2) * 1;
}

int ShakoBoard::castlingFile(WesternBoard::CastlingSide castlingSide) const
{
    Q_ASSERT(castlingSide != NoCastlingSide);
    return castlingSide == QueenSide ? 3 : width() - 3; // usually D and H
}

int ShakoBoard::castlingRank(Chess::Side castlingSide) const
{
    Q_ASSERT(castlingSide != Side::NoSide);
    return castlingSide == Side::Black ? 3 : height(); // usually 2 and 9
}

void ShakoBoard::generateMovesForPiece(QVarLengthArray< Move >& moves,
                          int pieceType,
                          int square) const
{

    WesternBoard::generateMovesForPiece(moves, pieceType, square);
    if (pieceHasMovement(pieceType, ElephantMovement)) {
        generateHoppingMoves(square, m_elephantOffsets, moves);
        generateHoppingMoves(square, m_ferzOffsets, moves);
    }
    if (pieceHasMovement(pieceType, CannonMovement)) { // Code taken from https://github.com/gaintpd/cutechess, the janggi adaptation
        QVarLengthArray< Move > testmoves;
        testmoves.clear();
        WesternBoard::generateMovesForPiece(testmoves, Rook, square);
        for (const auto m: testmoves)
            {
                const bool isCapture = captureType(m) != Piece::NoPiece;
                if (!isCapture) { moves.append(m); }
            }

        QVarLengthArray<int> cannonRelOffsets;
        cannonRelOffsets.clear();
        Side side = sideToMove();
        for (int i = 0; i < m_orthogonalOffsets.size(); i++)
        {
            int offset = m_orthogonalOffsets[i];
            int targetSquare = square + offset;

            Piece capture;
            int obstacle = 0;
            while(!(capture = pieceAt(targetSquare)).isWall())
            {
                if(capture.isEmpty())
                {
                    if(obstacle == 1)
                    {
                    }
                }
                else
                {
                    obstacle++;
                    if(obstacle == 2 && capture.side() != side)
                    {
                        cannonRelOffsets.append(targetSquare - square);
                        break;
                    }
                }
                targetSquare += offset;
            }
        }

        generateHoppingMoves(square, cannonRelOffsets, moves);
    }

    if (pieceType != Pawn)
        return;

    Side side = sideToMove();
    int sign = (side == Side::White) ? 1 : -1;
    int rank = chessSquare(square).rank();

    // add missing pawn double steps from third rank
    int rank3 = (side == Side::White) ? 2 : height() - 3;
    if (rank == rank3)
    {
        for (const PawnStep& pStep: m_pawnSteps)
        {
            if (pStep.type != FreeStep)
                continue;

            int targetSquare = square + pawnMoveOffset(pStep, sign);
            Piece capture = pieceAt(targetSquare);

            if (capture.isEmpty())
            {
                targetSquare += pawnMoveOffset(pStep, sign);
                capture = pieceAt(targetSquare);
                if (capture.isEmpty())
                    moves.append(Move(square, targetSquare));
            }
        }
    }
}

bool ShakoBoard::inCheck(Side side, int square) const
{
    Piece piece;
    Side opSide = side.opposite();
    if (square == 0)
        square = kingSquare(side);


    //Cannon checks from https://github.com/gaintpd/cutechess
    for (int i = 0; i < m_orthogonalOffsets.size(); i++)
    {
        int offset = m_orthogonalOffsets[i];
        int checkSquare = square + offset;

        int obstacle = 0;
        while (!(piece = pieceAt(checkSquare)).isWall())
        {
            if (!piece.isEmpty())
            {
                obstacle++;
                if (obstacle == 2 && piece.side() == opSide &&
                   pieceHasMovement(piece.type(), CannonMovement))
                    return true;
            }
            checkSquare += offset;
        }
    }
    // Elephant attacks from shatranj
    for (int i = 0; i < m_elephantOffsets.size(); i++)
    {
        int target = square + m_elephantOffsets[i];
        if (!isValidSquare(chessSquare(target)))
            continue;
        piece = pieceAt(target);
        if (piece.side() == opSide
        &&  pieceHasMovement(piece.type(), ElephantMovement))
            return true;
    }

    for (int i = 0; i < m_ferzOffsets.size(); i++)
    {
        piece = pieceAt(square + m_ferzOffsets[i]);
        if (piece.side() == opSide
        &&  pieceHasMovement(piece.type(), FerzMovement) | pieceHasMovement(piece.type(), ElephantMovement))
            return true;
    }

    return WesternBoard::inCheck(side, square);
}

void ShakoBoard::addPromotions(int sourceSquare,
				int targetSquare,
				QVarLengthArray<Move>& moves) const
{
	WesternBoard::addPromotions(sourceSquare, targetSquare, moves);

    moves.append(Move(sourceSquare, targetSquare, Cannon));
    moves.append(Move(sourceSquare, targetSquare, Elephant));
}

} // namespace Chess

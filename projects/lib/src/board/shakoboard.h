/*
    This file is part of Cute Chess.
    Copyright (C) 2008-2018 Cute Chess authors

    Cute Chess is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cute Chess is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cute Chess.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHAKOBOARD_H
#define SHAKOBOARD_H

#include "westernboard.h"

namespace Chess {

/*!
 * \brief A board for Shako
 *
 *  Shako is a chess variant created by Jean-Louis Cazaux.
 *  The game is played on a 10x10 board and introduces two new pieces, the cannon and elephant, which are both derived from Xiangqi (Chinese chess).
 *
 * \note Rules: https://www.chessvariants.com/large.dir/shako.html
 */
class LIB_EXPORT ShakoBoard : public WesternBoard
{
	public:
        /*! Creates a new ShakoBoard object. */
        ShakoBoard();

		// Inherited from WesternBoard
		virtual Board* copy() const;
		virtual QString variant() const;
		virtual int width() const;
        virtual int height() const;
		virtual QString defaultFenString() const;

	protected:
        /*! Special piece types for Shako variants. */
        enum ShakoPieceType
		{
            Cannon = 7,	//  Cannon
            Elephant	// Elephant
		};
        /*! Movement mask for Ferz move pattern. */
        static const unsigned FerzMovement = 16;
        /*! Movement mask for Elephant moves. */
        static const unsigned ElephantMovement = 32;
        /*! Movement mask for Wazir move pattern. */
        static const unsigned WazirMovement = 64;
        /*! Movement mask for Cannon moves. */
        static const unsigned CannonMovement = 128;

        virtual void vInitialize();
        virtual inline int pawnMoveOffset(const PawnStep& ps, int sign) const;
        virtual void generateMovesForPiece(QVarLengthArray< Move >& moves,
                           int pieceType,
                           int square) const;

		// Inherited from WesternBoard
		virtual void addPromotions(int sourceSquare,
					   int targetSquare,
					   QVarLengthArray<Move>& moves) const;
        virtual bool inCheck(Side side, int square) const;
        virtual int castlingFile(WesternBoard::CastlingSide castlingSide) const;
        virtual int castlingRank(Chess::Side castlingSide) const;

private:

    int m_arwidth;

    QVarLengthArray<int> m_ferzOffsets;
    QVarLengthArray<int> m_wazirOffsets;
    QVarLengthArray<int> m_elephantOffsets;
    QVarLengthArray<int> m_orthogonalOffsets;

};

} // namespace Chess
#endif // SHAKOBOARD_H

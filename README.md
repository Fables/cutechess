# Cute Chess - Adapted For Fairy

TODO:

- [ ] FIX 3fold check repetition for empire, synochess and shinobi

- [x] 3check (default cutechess)
- [ ] 3check-crazyhouse 
- [ ] 4x5chess
- [ ] 4x6chess
- [x] 5check (default cutechess)
- [ ] 5x6chess
- [ ] 6x6atom
- [ ] active
- [ ] advancedpawn
- [x] ai-wok (default cutechess)
- [x] almost (default cutechess)
- [x] amazon (default cutechess)
- [ ] andernach (on fork https://github.com/alwey/cutechess)
- [ ] anti-andernach (on fork https://github.com/alwey/cutechess)
- [ ] anti-losalamos 
- [x] antichess (default cutechess)
- [ ] armageddon 
- [x] asean (default cutechess)
- [ ] ataxx 
- [x] atomic (default cutechess)
- [ ] atomic-giveaway 
- [ ] atomic-giveaway-hill 
- [ ] berolina (on fork https://github.com/alwey/cutechess)
- [ ] breakthrough 
- [ ] bughouse 
- [ ] cambodian (on fork https://github.com/alwey/cutechess)
- [ ] cannibalclobber (on fork https://github.com/alwey/cutechess)
- [ ] cannibalclobber10 (on fork https://github.com/alwey/cutechess)
- [x] capablanca (default cutechess)
- [ ] capahouse 
- [ ] caparandom (on fork https://github.com/alwey/cutechess)
- [ ] capture
- [ ] captureall
- [ ] centaur 
- [x] cfour (default cutechess)
- [ ] chancellor (on fork https://github.com/alwey/cutechess) 
- [ ] changeover (on fork https://github.com/alwey/cutechess)
- [ ] chaturanga 
- [ ] checkless (on fork https://github.com/alwey/cutechess)
- [x] chess (default cutechess)
- [ ] chessgi (on fork https://github.com/alwey/cutechess) 
- [x] chigorin (default cutechess)
- [ ] clobber (on fork https://github.com/alwey/cutechess)
- [ ] clobber10 (on fork https://github.com/alwey/cutechess)
- [x] codrus (default cutechess)
- [ ] coffeehouse 
- [ ] coregal (https://github.com/alwey/cutechess)
- [ ] cornerrook 
- [x] courier (default cutechess)
- [x] crazyhouse (default cutechess)
- [ ] crossing
- [ ] diana
- [ ] displacedgrid (on fork https://github.com/alwey/cutechess)
- [x] dobutsu (default cutechess)
- [ ] doublearmy
- [x] embassy (default cutechess)
- [x] empire 
- [x] euroshogi (default cutechess) 
- [x] extinction (default cutechess)  
- [x] fischerandom (default cutechess)
- [ ] flipello 
- [ ] flipersi 
- [ ] gardner 
- [ ] gemini 
- [x] giveaway (default cutechess)
- [ ] gomoku (on fork https://github.com/alwey/cutechess)
- [ ] gomokufreestyle (on fork https://github.com/alwey/cutechess)
- [ ] gorogoro 
- [ ] gothhouse
- [ ] gothic (on fork https://github.com/alwey/cutechess) 
- [x] grand (default cutechess)
- [ ] grandhouse 
- [ ] grasshopper
- [ ] grid (on fork https://github.com/alwey/cutechess)
- [ ] gridolina (on fork https://github.com/alwey/cutechess)
- [x] gryphon (default cutechess)
- [ ] gustav3 (on fork https://github.com/alwey/cutechess)
- [x] hoppelpoppel (default cutechess)
- [x] horde (default cutechess)
- [ ] indiangreat
- [ ] janggi (already added on a fork https://github.com/gaintpd/cutechess/) 
- [ ] janggicasual (already added on a fork https://github.com/gaintpd/cutechess/)
- [ ] janggihouse (already added on a fork https://github.com/gaintpd/cutechess/)
- [ ] janggimodern (already added on a fork https://github.com/gaintpd/cutechess/)
- [ ] janggitraditional (already added on a fork https://github.com/gaintpd/cutechess/)
- [x] janus (default cutechess)
- [x] jesonmor (default cutechess)
- [x] judkins (default cutechess)
- [ ] karouk (on fork https://github.com/alwey/cutechess)
- [ ] kinglet (on fork https://github.com/alwey/cutechess)
- [x] kingofthehill (default cutechess)
- [x] knightmate (default cutechess)
- [ ] koedem 
- [ ] kyotoshogi 
- [x] loop (default cutechess)
- [x] losalamos (default cutechess)
- [x] losers (default cutechess)
- [ ] maharajah 
- [ ] maharajah2
- [ ] makpong (on fork https://github.com/alwey/cutechess)
- [x] makruk (default cutechess)
- [ ] makrukhouse 
- [ ] manchu 
- [ ] micro 
- [ ] microchess
- [ ] mini 
- [x] minishogi (default cutechess)
- [ ] minixiangqi (already added on a fork https://github.com/gaintpd/cutechess/) 
- [ ] modern (on fork https://github.com/alwey/cutechess)
- [x] newzealand (default cutechess)
- [ ] nightrider
- [ ] nocastle 
- [ ] nocheckatomic 
- [ ] normal 
- [ ] okisakishogi
- [x] orda
- [x] ordamirror
- [ ] pawnsmassacre
- [ ] pawnsonly 
- [ ] peasant 
- [x] placement (default cutechess) 
- [x] pocketknight (default cutechess)
- [x] racingkings (default cutechess)
- [ ] rifle (on fork https://github.com/alwey/cutechess)
- [ ] screen
- [x] seirawan (default cutechess)
- [ ] semitorpedo 
- [x] shako (also on fork https://github.com/gaintpd/cutechess/ didn't notice until later, adapted the castling code from it) 
- [ ] shatar 
- [x] shatranj (default cutechess)
- [x] shinobi
- [x] shogi (default cutechess)
- [ ] shogun 
- [ ] shouse 
- [x] sittuyin (default cutechess)
- [ ] simplifiedgryphon (on fork https://github.com/alwey/cutechess)
- [ ] slippegrid (on fork https://github.com/alwey/cutechess)
- [ ] supply
- [ ] superandernach (on fork https://github.com/alwey/cutechess)
- [x] synochess
- [ ] tencubed
- [x] threekings (default cutechess)
- [ ] twokings (on fork https://github.com/alwey/cutechess)
- [ ] twokingssymetric (on fork https://github.com/alwey/cutechess) 
- [ ] tictactoe (on fork https://github.com/alwey/cutechess)
- [ ] torishogi
- [ ] upsidedown (very irrelevant, can be done with just different FEN start) 
- [ ] weak 
- [ ] wildebeest
- [ ] xiangqi (already added on a fork https://github.com/gaintpd/cutechess/)
- [ ] xiangqihouse (already added on a fork https://github.com/gaintpd/cutechess/)
- [ ] yarishogi

# Cute Chess

![GitHub CI](https://github.com/cutechess/cutechess/workflows/build%20cutechess/badge.svg)

Cute Chess is a graphical user interface, command-line interface and a library
for playing chess. Cute Chess is written in C++ using the [Qt
framework](https://www.qt.io/).

## Installation

### Binaries

See the [Releases](https://github.com/cutechess/cutechess/releases) page.

### Building from source

Cute Chess requires Qt 5.11 or greater, a compiler with C++11 support and `qmake`.
Cute Chess depends on the following Qt 5 modules:

* qt5-widgets
* qt5-svg
* qt5-concurrent
* qt5-printsupport
* qt5-testlib (optional: unit tests)

Run these commands:

    $ qmake
    $ make

If you are using the Visual C++ compiler replace `make` with `nmake`.

Documentation is available as Unix manual pages in the `docs/` directory.

For detailed build instruction for various operating systems, see the
[Building from source](https://github.com/cutechess/cutechess/wiki/Building-from-source)
wiki page.

## Running

The `cutechess` program is the graphical user interface of Cute Chess.
It can be run either from command-line or from your desktop environment's
application launcher.

The `cutechess-cli` program is the command-line interface for playing
games between chess engines. For example, to play ten games between two Sloppy
engines (assuming `sloppy` is in PATH) with a time control of 40 moves in 60
seconds:

    $ cutechess-cli -engine cmd=sloppy -engine cmd=sloppy -each proto=xboard tc=40/60 -rounds 10

See `cutechess-cli -help` for descriptions of the supported options or manuals
for full documentation.

## License

Cute Chess is released under the GPLv3+ license except for the components in
the `projects/lib/components` and `projects/gui/components` directories which
are released under the MIT License.

## Credits

Cute Chess was written by Ilari Pihlajisto, Arto Jonsson and [contributors](https://github.com/cutechess/cutechess/graphs/contributors)
